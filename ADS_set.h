#ifndef ADS_SET_H
#define ADS_SET_H

#include <functional>
#include <algorithm>
#include <iostream>
#include <stdexcept>

/**
 * B+Tree by Bernhard Frick
 *
 * Algorithms and Data Structures
 * Faculty of Computer Science
 * University of Vienna
 *
 * For Reference, see Specification:
 * @see http://en.cppreference.com/w/cpp/container/set
 * @see https://cewebs.cs.univie.ac.at/ADS/ss17/index.php?m=D&t=unterlagen&c=show&CEWebS_what=Spezifikation
 *
 * In the C++11-Standard, a null pointer is converted to false implicitly, and a non-null pointer is converted to true.
 *
 * @tparam Key  The Data Type that should be stored in the B+Tree. Must be set when compiling using -DETYPE=<Key>
 * @tparam N    The Size of one Node. Can be set when compiling using -DSIZE=<N>
 */
template <typename Key, size_t N = 2>
class ADS_set {
public:
    class Iterator;
    using value_type = Key;
    using key_type = Key;
    using reference = key_type&;
    using const_reference = const key_type&;
    using size_type = size_t;
    using difference_type = std::ptrdiff_t;
    using iterator = Iterator;
    using const_iterator = Iterator;
    using key_compare = std::less<key_type>;
    using key_equal = std::equal_to<key_type>;

private:

    /** Number of Keys held in the B+Tree **/
    size_type elements;

    /** Type of a Node **/
    enum NodeType {InternalNode, LeafNode};

    /** A Node within the B+Tree **/
    class Node {
    public:

        /** Node-Type (InternalNode, LeafNode) **/
        NodeType type;

        /** Number of used Elements in Node **/
        size_type used;

        /** Value-Array **/
        key_type values[2*N];

        /** ChildNode-Array **/
        Node* children[2*N+1];

        /** Previous Leaf-Pointer **/
        Node* previous;

        /** Next Leaf-Pointer **/
        Node* next;

        /** Constructor **/
        Node(NodeType type): type(type), used{0}, values{}, children{}, previous{nullptr}, next{nullptr} {}

        /** Destructor **/
        ~Node() {
            if (type == InternalNode) for (size_t i = 0; i < used+1; ++i) delete children[i];
        }

    };

    /** The Root-Node of the B+Tree **/
    Node* rootNode;

    /**
     * Recursively inserts the given Key into the B+Tree.
     * This Method resolves overflows in Internal- and Leaf-Nodes.
     * @param node  The Node in which to insert the given key
     * @param key   The Key to insert
     * @return      A Node-Pointer to a newly created Node, if a Node-split occurred, nullptr otherwise
     */
    Node* add(Node* node, key_type key) {

        if (node->type == NodeType::InternalNode) {

            /** Finding the Position to insert **/
            size_t insertPos = 0;
            for (; insertPos < node->used; ++insertPos)
            {
                if (key_compare{}(key, node->values[insertPos])) break; /** pos found **/
            }

            /** Calling add() for Child-Node and keeping the return-Value **/
            Node* retNode = this->add(node->children[insertPos], key);

            /** Stopping if there is no returned Node to handle **/
            if (retNode == nullptr) return nullptr;

            /** A Node was returned, insert retNode into node **/

            if (node->used == 2*N) { /** InternalNode is full --> split **/

                /** Creating a new temporary Value-Array (size: 2*N+1) **/
                Key tempV[2*N+1] = {};

                /** Creating a new temporary Node-Array (size: 2*N+2) **/
                Node* tempN[2*N+2] = {};

                /** Moving Values from node to tempV **/
                for (size_t i = 0; i < 2*N; ++i)
                {
                    tempV[i] = node->values[i];
                }

                /** Moving Node* from node to tempN **/
                for (size_t i = 0; i < 2*N+1; ++i)
                {
                    tempN[i] = node->children[i];
                }
                node->used = 0;

                /** Finding Insert-Position and inserting Reference-Value in tempV **/
                size_t i = 2*N;
                for (; i > 0 && key_compare{}(retNode->values[0], tempV[i-1]); --i)
                {
                    tempV[i] = tempV[i-1];
                }
                tempV[i] = retNode->values[0];

                /** Inserting retNode at insertPos in tempN **/
                size_t j = 2*N+1;
                for (; j > i+1; --j)
                {
                    tempN[j] = tempN[j-1];
                }
                tempN[j] = retNode;

                /** Creating a new Neighbor Internal Node **/
                Node* r = new Node(NodeType::InternalNode);

                /** Inserting left half of the temporary Arrays in node and updating node->used **/
                for (size_t i = 0; i < N; ++i) /** Values **/
                {
                    node->values[i] = tempV[i];
                    ++node->used;
                }
                for (size_t i = 0; i <= N; ++i) /** Nodes **/
                {
                    node->children[i] = tempN[i];
                }

                /** Inserting right half of the temporary Arrays in r and updating r->used **/
                for (size_t i = N; i < 2*N+1; ++i) /** Values **/
                {
                    r->values[r->used++] = tempV[i];
                }
                size_t c = 0;
                for (size_t i = N+1; i < 2*N+2; ++i) /** Nodes **/
                {
                    r->children[c++] = tempN[i];
                }

                /** Removing Reference Value from retNode, if retNode is an Internal Node **/
                if (retNode->type == NodeType::InternalNode) {

                    for (size_t i = 0; i < retNode->used; ++i)
                    {
                        retNode->values[i] = retNode->values[i+1];
                    }
                    --retNode->used;

                }

                /** Returning newly created Node **/
                return r;

            } else { /** InternalNode is not full --> insert **/

                /** Finding Position and moving Values to create a gap for the Value to hoist **/
                insertPos = node->used;
                for (; insertPos > 0 && key_compare{}(retNode->values[0], node->values[insertPos-1]); --insertPos)
                {
                    node->values[insertPos] = node->values[insertPos-1];
                }

                /** Hoisting Reference Value **/
                node->values[insertPos] = retNode->values[0];
                ++node->used;

                /** Moving Pointers to create a gap for retNode **/
                for (size_t i = node->used; i > insertPos; --i)
                {
                    node->children[i] = node->children[i-1];
                }

                /** Inserting retNode in node **/
                node->children[insertPos+1] = retNode;

                /** Removing Reference Value from retNode, if retNode is an Internal Node **/
                if (retNode->type == NodeType::InternalNode) {

                    for (size_t i = 0; i < retNode->used-1; ++i)
                    {
                        retNode->values[i] = retNode->values[i+1];
                    }
                    --retNode->used;

                }

                return nullptr;

            }

        } else { /** Node is LeafNode **/

            /** Finding the Position to insert **/
            /** Also, if a duplicate is found, Insertion can be stopped. **/
            size_t insertPos = 0;
            for (; insertPos < node->used; ++insertPos)
            {
                if (key_equal{}(node->values[insertPos], key)) return nullptr; /** duplicate **/
                if (key_compare{}(key, node->values[insertPos])) break; /** pos found **/
            }

            if (node->used == 2*N) { /** LeafNode is full --> split **/

                /** Creating a new temporary Value-Array (size: 2*N+1) **/
                Key tempV[2*N+1] = {};

                /** Moving Values from node to the temporary Array **/
                for (size_t i = 0; i < 2*N; ++i)
                {
                    tempV[i] = node->values[i];
                }
                node->used = 0;

                /** Inserting new Value into the temporary Array **/
                for (size_t i = 2*N; i > insertPos; --i)
                {
                    tempV[i] = tempV[i - 1];
                }
                tempV[insertPos] = key;

                /** Creating a new Neighbor Leaf Node **/
                Node* r = new Node(NodeType::LeafNode);

                /** Inserting left half of the temporary Array in node and updating node->used **/
                for (size_t i = 0; i < N; ++i)
                {
                    node->values[i] = tempV[i];
                    ++node->used;
                }

                /** Inserting right half of the temporary Array in r and updating r->used **/
                for (size_t i = N; i < 2*N+1; ++i)
                {
                    r->values[r->used++] = tempV[i];
                }

                /** Updating chaining of Leaf Nodes **/
                r->next = node->next;
                r->previous = node;
                if (node->next) node->next->previous = r;
                node->next = r;

                /** Incrementing Counter **/
                ++this->elements;

                /** Returning newly created Node **/
                return r;

            } else { /** LeafNode is not full --> insert **/

                /** Moving values to the right to create a gap for the new Value **/
                for (size_t i = node->used; i > 0 && key_compare{}(key, node->values[i-1]); --i)
                {
                    node->values[i] = node->values[i-1];
                }

                /** Inserting new Value **/
                node->values[insertPos] = key;
                ++node->used;

                /** Incrementing Counter **/
                ++this->elements;

                return nullptr;
            }

        }

    }

    /**
     * Inserts the given Key into the Root-Node of the B+Tree using add().
     * This method resolves overflows that occur inside the root-Node.
     * @param key   The Key to insert
     */
    void add_to_root(key_type key) {

        /** Inserting the given key into the Root-Node of the B+Tree **/
        Node* retNode = this->add(this->rootNode, key);

        /** Checking for Root-Overflow **/
        if (retNode == nullptr) return;

        /** Is Root-Node LeafNode or InternalNode? **/
        if (this->rootNode->type == NodeType::LeafNode) {

            /** Creating a new Root Node **/
            Node* r = new Node(NodeType::InternalNode);

            /** Linking the old Root Node and the new Node in the new Root Node **/
            r->children[0] = this->rootNode;
            r->children[1] = retNode;

            /** Updating the Reference Value inside the new Root Node **/
            r->values[0] = retNode->values[0];
            ++r->used;

            /** Linking the new Root Node in this **/
            this->rootNode = r;

        } else if (this->rootNode->type == NodeType::InternalNode) {

            /** Creating a new Internal Root Node **/
            Node* r = new Node(NodeType::InternalNode);

            /** Updating Reference Value in new Root **/
            r->values[0] = retNode->values[0];
            ++r->used;

            /** Removing first Value from retNode, because retNode is an Internal Node **/
            for (size_t i = 0; i < retNode->used-1; ++i)
            {
                retNode->values[i] = retNode->values[i+1];
            }
            --retNode->used;

            /** Linking old Root-Node and retNode in new Root **/
            r->children[0] = this->rootNode;
            r->children[1] = retNode;

            /** Linking new Root in ADS_set **/
            this->rootNode = r;

        }

    }

    /**
     * Resolves an underflow that occurred after removing a value from a node.
     * An underflow is resolved by shifting a Value from or merging with a
     * Neighbor-Node that has the same Parent-Node. In order to allow access
     * to the Parent-Node, the underflow-resolution-algorithm is designed to
     * resolve an underflow from the scope of the Parent-Node.
     * @param node          The Parent-Node of a Node in which an Underflow occurred
     * @param underflowPos  The Position of the Underflow-Node in node
     */
    void fix_underflow(Node* node, size_type underflowPos) {

        if (underflowPos > 0 && node->children[underflowPos-1]->used > N) { /** Shift from Left Neighbor **/

            /** Defining Shift-Source and Target **/
            Node* source = node->children[underflowPos-1];
            Node* target = node->children[underflowPos];

            /** Moving all Values and Pointers in center by 1 to the right **/
            for (size_type i = target->used+1; i > 0; --i)
            {
                target->values[i] = target->values[i-1]; /** Moving Values **/
                target->children[i] = target->children[i-1]; /** Moving Pointers **/
            }
            ++target->used;

            if (target->type == NodeType::LeafNode) {

                /** Copying Value from source to target **/
                target->values[0] = source->values[source->used-1];

                /** Updating Reference Value **/
                node->values[underflowPos-1] = target->values[0];

            } else {

                /** Pulling Reference-Value down **/
                target->values[0] = node->values[underflowPos-1];

                /** Moving Reference-Value up **/
                node->values[underflowPos-1] = source->values[source->used-1];

                /** Shifting Child-Node **/
                target->children[0] = source->children[source->used];

            }

            /** Removing copied value from source **/
            --source->used;

        } else if (underflowPos < node->used && node->children[underflowPos+1]->used > N) { /** Shift from Right Neighbor **/

            /** Defining Shift-Source and Target **/
            Node* target = node->children[underflowPos];
            Node* source = node->children[underflowPos+1];

            if (target->type == NodeType::InternalNode) {

                /**Pulling down Reference Value from node to target **/
                target->values[target->used++] = node->values[underflowPos];

                /** Moving Reference Value up from source to node **/
                node->values[underflowPos] = source->values[0];

                /** Moving Pointer from source to target **/
                target->children[target->used] = source->children[0];

                /** Closing Gap in source **/
                size_type i = 0;
                for (; i < source->used-1; ++i)
                {
                    source->values[i] = source->values[i+1];
                    source->children[i] = source->children[i+1];
                }
                --source->used;

                /** Moving the missing last Pointer **/
                source->children[i] = source->children[i+1];


            } else { /** Child-Node is LeafNode **/

                /** Moving Reference Value from source to target **/
                target->values[target->used++] = source->values[0];

                /** Closing Gap in source **/
                for (size_type i = 0; i < source->used-1; ++i)
                {
                    source->values[i] = source->values[i+1];
                }
                --source->used;

                /** Updating Reference in node **/
                node->values[underflowPos] = source->values[0];

            }

        } else { /** Merge Nodes **/

            /** Always merge with right Neighbor, this simplifies the merge-algorithm **/
            size_type targetPos = underflowPos;
            if (targetPos == node->used) --targetPos;
            size_type sourcePos = targetPos+1;

            /** Defining Shift-Source and Target **/
            Node* target = node->children[targetPos];
            Node* source = node->children[sourcePos];

            /** Pull down reference value, if child Node is InternalNode **/
            if (target->type == NodeType::InternalNode) target->values[target->used++] = node->values[sourcePos-1];

            /** Moving all Values and Pointers from source Node to target Node **/
            /** Important: writing at the same pos in values and children because of pulled-down reference **/
            size_type i;
            for (i = 0; i < source->used; ++i)
            {
                target->values[target->used] = source->values[i];
                target->children[target->used] = source->children[i];
                ++target->used;
            }

            /** Add last Pointer from source, that is missing after the loop above **/
            target->children[target->used] = source->children[i];

            /** Updating node-chaining **/
            target->next = source->next;
            if (source->next) source->next->previous = target;

            /** Deleting empty source Node **/
            source->type = NodeType::LeafNode; /** Prevent Deletion of Child-Nodes **/
            delete source;

            /** Closing Gap in node **/
            for (i = targetPos; i < node->used - 1; ++i) {
                node->values[i] = node->values[i + 1];
                node->children[i + 1] = node->children[i + 2];
            }
            --node->used;

        }

    }

    /**
     * Resolves an underflow that occurred inside the Root-Node. The Root-Node can
     * have at least 1 value and 2 Child-Nodes. If there is an Underflow, the
     * height of the B+Tree decreases.
     */
    void fix_root_underflow() {

        /** The only Child-Node of the Root-Node becomes the new Root-Node **/
        Node* newRoot = this->rootNode->children[0];

        /** Delete the old Root-Node **/
        this->rootNode->type = NodeType::LeafNode;  /** Prevent Deletion of Child-Nodes **/
        delete this->rootNode;

        /** Setting the new Root-Node **/
        this->rootNode = newRoot;

    }

    /**
     * Recursively removes the given key from the given node.
     * @param node  The Node, from which key should be removed
     * @param key   The key to be removed
     * @return      True if key was removed, false otherwise (if key was not there in the first place)
     */
    bool remove(Node* node, const key_type& key) {

        /** Calculating the position for recursion. **/
        size_t recurPos = 0;
        for (; recurPos < node->used; ++recurPos)
        {
            if (key_compare{}(key, node->values[recurPos])) break; /** Position for recursion found **/
        }

        if (node->children[recurPos]->type == NodeType::InternalNode) {

            /** Removing key from children **/
            bool removed = this->remove(node->children[recurPos], key);

            /** Fixing underflow in child -- Child is always Internal **/
            if (node->children[recurPos]->used < N) this->fix_underflow(node, recurPos);

            /** Returning Removal-Information **/
            return removed;

        } else { /** Node is LeafNode **/

            /** Calculating the position of the value to delete **/
            size_t deletePos = 0;
            for (; deletePos < node->children[recurPos]->used; ++deletePos)
            {
                if (key_compare{}(node->children[recurPos]->values[deletePos], key)) continue; /** Key not yet found **/
                if (key_equal{}(key, node->children[recurPos]->values[deletePos])) break; /** deletePos found **/
                if (key_compare{}(key, node->children[recurPos]->values[deletePos])) return false; /** Stop if key does not exist **/
            }

            /** Stop if loop ran over the end **/
            if (deletePos == node->children[recurPos]->used) return false;

            /** Deleting Key from LeafNode at deletePos **/
            for (size_t i = deletePos; i < node->children[recurPos]->used-1; ++i) {
                node->children[recurPos]->values[i] = node->children[recurPos]->values[i+1];
            }
            --node->children[recurPos]->used;
            --this->elements;

            /** Fixing underflow in child -- Child is always Leaf **/
            if (node->children[recurPos]->used < N) this->fix_underflow(node, recurPos);

            /** Returning Removal-Information **/
            return true;

        }

    }

    /**
     * Removes the given Key from the Root-Node of the B+Tree.
     * Because a Root-Node has at least 1 Value and 2 Child-Nodes, removal is
     * slightly different and therefore handled separately.
     * @param key   The Key to remove from the B+Tree
     * @return      1 if key was removed, 0 otherwise (if key was not there in the first place)
     */
    size_type remove_from_root(const key_type& key) {

        /** Stop if the B+Tree is empty **/
        if (this->elements == 0) return 0;

        if (this->rootNode->type == NodeType::InternalNode) {

            /** Calling remove on rootNode **/
            bool removed = this->remove(this->rootNode, key);

            /** Fixing Underflow in rootNode **/
            if (this->rootNode->used == 0) this->fix_root_underflow();

            /** Returning Removal-Information **/
            return (size_type) removed;

        } else { /** Node is LeafNode **/

            /** Calculating the position of the value to delete **/
            size_t deletePos = 0;
            for (; deletePos < this->rootNode->used; ++deletePos)
            {
                if (key_compare{}(this->rootNode->values[deletePos], key)) continue; /** Key not yet found **/
                if (key_equal{}(key, this->rootNode->values[deletePos])) break; /** deletePos found **/
                if (key_compare{}(key, this->rootNode->values[deletePos])) return 0; /** Stop if key does not exist **/
            }

            /** Stop if loop ran over the end **/
            if (deletePos == this->rootNode->used) return 0;

            /** Deleting Key from RootLeafNode **/
            for (size_t i = deletePos; i < this->rootNode->used-1; ++i) {
                this->rootNode->values[i] = this->rootNode->values[i+1];
            }
            --this->rootNode->used;
            --this->elements;

            /** Returning Removal-Information **/
            return 1;

        }

    }

    /**
     * Recursively searches the given Node for the given Key.
     * @param node  The Node to search in
     * @param key   The Key to be searched
     * @return      An Iterator to Key
     */
    Iterator search(Node* node, const key_type& key) const {

        if (node->type == NodeType::InternalNode) { /** Node is InternalNode **/

            /** Finding Position for recursive Call **/
            size_type i = 0;
            for (; i < node->used; ++i)
            {
                if (key_compare{}(key, node->values[i])) break;
            }

            /** Call search() at recurPos **/
            return this->search(node->children[i], key);

        } else { /** Node is LeafNode **/

            /** Loop over all Values in the Leaf and return Iterator to key **/
            for (size_type i = 0; i < node->used; ++i)
            {
                if (key_equal{}(key, node->values[i])) return const_iterator{node, i};
            }

            /** Key was not found, return End-Iterator **/
            return this->end();

        }

    }

    /**
     * Finds the first Leaf-Node inside the given node
     * @param node  The Node to search in
     * @return      A Pointer to the first Leaf-Node
     */
    Node* first(Node* node) const {
        if (node->type == LeafNode) return node;
        else return this->first(node->children[0]);
    }

    /**
     * Recursively displays the Structure of the B+Tree.
     * @param o     An Output-Stream to write to
     * @param node  A Node that should be displayed
     * @param level The recursion-Depth, used to represent the Structure of the B+Tree with indentation
     */
    void print(std::ostream& o, Node* node, size_t level) const {

        if (node->type == NodeType::LeafNode) {

            /** Indenting **/
            for (size_t j = 0; j < level; ++j) o << "    ";

            /** Printing Node Information **/
            o << "LeafNode " << node->previous << "--" << node << "--" << node->next << " - " << node->used << " Values: ";

            /** Printing Values **/
            for (size_t i = 0; i < node->used; ++i) o << node->values[i] << " ";

            /** Line-Break **/
            o << std::endl;

        } else if (node->type == NodeType::InternalNode) {

            /** Indenting **/
            for (size_t j = 0; j < level; ++j) o << "    ";

            /** Printing Node Information **/
            o << "InternalNode (" << node << ") - " << node->used << " Values: ";

            /** Printing Values **/
            for (size_t i = 0; i < node->used; ++i) o << node->values[i] << " ";

            o << "- " << node->used+1 << " Children: ";

            /** Printing Children **/
            for (size_t i = 0; i < node->used+1; ++i) o << node->children[i] << " ";

            /** Line-Break **/
            o << std::endl;

            /** Calling Print for all Child-Nodes **/
            for (size_t i = 0; i < node->used+1; ++i) this->print(o, node->children[i], level+1);

        }

    }

public:

    /**
     * Default Constructor
     */
    ADS_set(): elements{0} {
        this->rootNode = new Node(NodeType::LeafNode);
    }

    /**
     * InitializerList-Constructor
     * @param ilist     The List to initialize the ADS_set with.
     */
    ADS_set(std::initializer_list<key_type> ilist): elements{0} {
        this->rootNode = new Node(NodeType::LeafNode);
        this->insert(ilist);
    }

    /**
     * Range-Constructor
     * @tparam InputIt  The Iterator-Type to use.
     * @param first     The first element of the range to insert
     * @param last      The last element of the range to insert
     */
    template<typename InputIt>
    ADS_set(InputIt first, InputIt last): elements{0} {
        this->rootNode = new Node(NodeType::LeafNode);
        this->insert(first, last);
    }

    /**
     * Copy-Constructor
     * @param other An ADS_set with which this should be initialized
     */
    ADS_set(const ADS_set& other): elements{0} {
        this->rootNode = new Node(NodeType::LeafNode);
        *this = other;
    }

    /**
     * Destructor
     */
    ~ADS_set() {
        delete rootNode;
    }

    /**
     * Copy-Assignment-Operator
     * @param other An ADS_set with which this should be overwritten
     * @return      A Reference to this
     */
    ADS_set& operator=(const ADS_set& other) {

        /** Stop, if self-assignment **/
        if (this == &other) return *this;

        /** Not equal, clearing this **/
        this->clear();

        /** Iterating over the given Set and inserting every Value in this **/
        for (auto it = other.begin(); !(it == other.end()); ++it)
        {
            this->insert(*it.operator->());
        }

        /** Returning this **/
        return *this;
    }

    /**
     * Initializer-List-Assignment-Operator
     * @param ilist An Initializer-List with which this should be overwritten
     * @return      A Reference to this
     */
    ADS_set& operator=(std::initializer_list<key_type> ilist) {

        /** Clearing this **/
        this->clear();

        /** Inserting ilist using insert-Metod **/
        this->insert(ilist);

        /** Returning this **/
        return *this;
    }

    /**
     * Retrieves the number of Keys stored in the B+Tree
     * @return      The number of Keys
     */
    size_type size() const {
        return this->elements;
    }

    /**
     * Checks whether the B+Tree is empty.
     * @return      Bool true if the B+Tree contains 0 elements, false otherwise
     */
    bool empty() const {
        return this->elements == 0;
    }

    /**
     * Returns the number of Keys matching the given key.
     * @param key   The Key to count
     * @return      Number of Keys that compare equivalent to key (either 1 or 0)
     */
    size_type count(const key_type& key) const {
        return (this->find(key) == end()) ? 0 : 1;
    }

    /**
     * Finds a Key equivalent to the given key.
     * @param key   The Key to search for
     * @return      Iterator to Key. If no Key is found, past-the-end (see end()) iterator is returned.
     */
    iterator find(const key_type& key) const {
        return search(this->rootNode, key);
    }

    /**
     * Removes all Keys from the B+Tree.
     */
    void clear() {

        /** Deleting all Nodes */
        delete this->rootNode;

        /** Resetting Element-Count **/
        this->elements = 0;

        /** Creating a new Root-Node **/
        this->rootNode = new Node(NodeType::LeafNode);

    }

    /**
     * Exchanges the contents of the B+Tree with those of other.
     * @param other ADS_set to exchange the contents with
     */
    void swap(ADS_set& other) {

        Node* tempNode = this->rootNode;
        size_type tempElements = this->elements;

        this->rootNode = other.rootNode;
        this->elements = other.elements;

        other.rootNode = tempNode;
        other.elements = tempElements;

    }

    /**
     * Inserts elements from initializer list ilist.
     * @param ilist The initializer list to insert the Keys from
     */
    void insert(std::initializer_list<key_type> ilist) {
        for (auto value: ilist) this->add_to_root(value);
    }

    /**
     * Insert key into the B+Tree.
     * @param key   The Key to insert.
     * @return      A pair consisting of an Iterator and a boolean.
     *              Iterator:   Points to the inserted Key, or to the already existing key.
     *              Boolean:    True, if key was inserted, false otherwise (if it already existed).
     */
    std::pair<iterator,bool> insert(const key_type& key) {

        /** Key was not yet inserted **/
        bool inserted = false;

        /** Searching for key **/
        Iterator it = this->find(key);

        /** If Key was not found... **/
        if (it == end()) {

            /** Inserting Key **/
            this->add_to_root(key);

            /** Updating Iterator **/
            it = this->find(key);

            /** Key was inserted **/
            inserted = true;

        }

        /** Returning Insertion-Data **/
        return std::make_pair(it, inserted);

    }

    /**
     * Inserts Keys from range [first, last) into the B+Tree.
     * @tparam InputIt  Iterator-Template
     * @param first     Iterator to the first Key of the Range to insert
     * @param last      Iterator to the last Key of the Rang to insert
     */
    template <typename InputIt>
    void insert(InputIt first, InputIt last) {
        for (auto it = first; it != last; ++it) this->add_to_root(*it);
    }

    /**
     * Removes the Key (if one exists) equivalent to key.
     * @param key   The Key to remove
     * @return      The Number of Keys removed ( 0 or 1)
     */
    size_type erase(const key_type& key) {
        return this->remove_from_root(key);
    }

    /**
     * Returns an Iterator to the first Key of the B+Tree.
     * @return      Iterator to the first Key, or End-Iterator if the container is empty.
     */
    const_iterator begin() const {
        if (this->empty()) return end();
        else return const_iterator{this->first(this->rootNode), 0};
    }

    /**
     * Returns an Iterator to the virtual element after the last Key of the B+Tree.
     * @return Iterator to the Key after the last Key
     */
    const_iterator end() const {
        return const_iterator{nullptr, 0};
    }

    /**
     * Recursively prints the whole B+Tree vor visual inspection.
     * @param o An Output Stream to dump the Structure onto
     */
    void dump(std::ostream& o = std::cerr) const {
        this->print(o, this->rootNode, 0);
    }

    /**
     * Equal-To-Operator - Compares the contents of both given B+Trees.
     * @param lhs   The left B+Tree of the comparison
     * @param rhs   The right B+Tree of the comparison
     * @return      True if the contents of both B+Trees are identical, false otherwise
     */
    friend bool operator==(const ADS_set& lhs, const ADS_set& rhs) {

        /** Not equal, if size is not equal **/
        if (lhs.elements != rhs.elements) return false;

        /** Iterators to both Containers **/
        auto lhs_it = lhs.begin();
        auto rhs_it = rhs.begin();

        /** End-Iterator **/
        auto lhs_end = lhs.end();

        /** Iterate over all Elements of both Containers **/
        for (;!(lhs_it == lhs_end);) {

            /** Stop if the keys are not equal **/
            if (!key_equal{}(*lhs_it, *rhs_it)) return false;

            /** Increment both Iterators **/
            ++lhs_it;
            ++rhs_it;

        }

        /** All Values are equal, therefore the Containers are equal. **/
        return true;
    }

    /**
     * Not-Equal-To-Operator - Compares the contents of both given B+Trees
     * @param lhs   The left B+Tree of the comparison
     * @param rhs   The right B+Tree of the comparison
     * @return      False if the contents of both B+Trees are identical, true otherwise
     */
    friend bool operator!=(const ADS_set& lhs, const ADS_set& rhs) {
        return !(lhs == rhs);
    }
};

/**
 * Iterator for ADS_set
 * @tparam Key  The Data Type that should be stored in the B+Tree. Must be set when compiling using -DETYPE=<Key>
 * @tparam N    The Size of one Node. Can be set when compiling using -DSIZE=<N>
 */
template <typename Key, size_t N>
class ADS_set<Key,N>::Iterator {
    Node* node;
    size_type position;
public:
    using value_type = Key;
    using difference_type = std::ptrdiff_t;
    using reference = const value_type&;
    using pointer = const value_type*;
    using iterator_category = std::forward_iterator_tag;

    explicit Iterator(Node* node, size_type position): node(node), position(position) {
    }

    reference operator*() const {
        return this->node->values[position];
    }

    pointer operator->() const {
        return &this->node->values[position];
    }

    Iterator& operator++() {
        if (this->position == this->node->used-1) {
            this->node = this->node->next;
            position = 0;
        } else {
            ++this->position;
        }
        return *this;
    }

    Iterator operator++(int) {
        Iterator tmp{*this};
        ++*this;
        return tmp;
    }

    friend bool operator==(const Iterator& lhs, const Iterator& rhs) {
        return lhs.node == rhs.node && lhs.position == rhs.position;
    }

    friend bool operator!=(const Iterator& lhs, const Iterator& rhs) {
        return !(lhs == rhs);
    }

};

/**
 * Swap-Operation for ADS_set
 * @tparam Key  The Data Type that should be stored in the B+Tree. Must be set when compiling using -DETYPE=<Key>
 * @tparam N    The Size of one Node. Can be set when compiling using -DSIZE=<N>
 * @param lhs   The left ADS_set
 * @param rhs   The right ADS_set
 */
template <typename Key, size_t N>
void swap(ADS_set<Key,N>& lhs, ADS_set<Key,N>& rhs) {
    lhs.swap(rhs);
}

#endif // ADS_SET_H
