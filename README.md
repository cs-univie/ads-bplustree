B+Tree [![build status](https://gitlab.com/sirbjoern/bplustree/badges/master/build.svg)](https://gitlab.com/sirbjoern/bplustree/commits/master)
===================================

An implementation of the B+Tree data structure for the Algorithm & Data Structures course at the University of Vienna.

The base ADS_Set.h was provided with the assignment together with simpletest and btest.

simpletest
----------

This simple interactive program provides methods to test the functionality of your implementation.
Every operation is also applied to an [std::set](http://en.cppreference.com/w/cpp/container/set).
Operations with return values are compared with the return value of std::set, and if the results differ an error is displayed.

__Compilation__

    $ g++ -Wall -Wextra -O3 --std=c++11 --pedantic-errors -DSIZE=2 -DETYPE=Person simpletest.C -o simpletest

__Usage__

    $ ./simpletest

__Help__

    new ...................... create new set
    dump ..................... call dump()
    delete ................... delete set
    iinsert <keys> ........... insert <keys>, call insert(InputIt, InputIt)
    insert <keys> ............ insert <keys>, call insert(const key_type&)
    iterator ................. iterator/find test
    erase [<keys>] ........... erase all keys (no output) or <keys>
    find [<keys>] ............ call find() for all keys (no output) or <keys>
    count [<keys>] ........... call count() for all keys (no output) or <keys>
    clear .................... call clear()
    size ..................... call size()
    empty .................... call empty()
    trace .................... toggle tracing on/off
    fiinsert <filename> ...... insert values from file <filename>, call insert(InputIt, InputIt)
    finsert <filename> ....... insert values from file <filename>, call insert(const key_type&)
    ferase <filename> ........ erase values from file <filename>
    riinsert [<n> [<seed>]] .. insert <n> random values, call insert(InputIt, InputIt), optionally reset generator to <seed>
    rinsert [<n> [<seed>]] ... insert <n> random values, call insert(const key_type&), optionally reset generator to <seed>
    rerase [<n> [<seed>]] .... erase <n> random values, optionally reset generator to <seed>
    quit (or EOF) ............ quit program (deletes set)
    list ..................... list all intended elements (sorted)
    help ..................... list of commands
    ? ........................ like 'help'

    arguments surrounded by [] are optional

btest
-----

The btest performs automated tests on the implementation. It will try to find a simple way to mess an ADS_set up.
Theoretically it should touch upon the full functionality, but is of course not capable of finding all possible error cases.

__Compilation__

    $ g++ -Wall -Wextra -Og -g -std=c++11 -pedantic-errors -pthread btest.cpp -o btest

__Usage__

    $ ./btest

__Help__

    -n $value ... number of values for first test, default: 10
    -m $value ... stepsize for n, default: 10
    -o $value ... maximum value for n, default: 100
    -v $value ... maximum value for first test, default: 10
    -w $value ... stepsize for v, default: 10
    -x $value ... maxmimum value for v, default: 100
    -s $value ... first seed, default: 666
    -t $value ... number of seeds (will be drawn from rng w/ previous seed), default: 1
                  will do the full test suite t times!
    -h        ... this message

valgrind
--------

[Valgrind](http://valgrind.org/) can automatically detect many memory management and threading bugs, and profile your programs in detail.

__Installation__

    $ sudo apt update
    $ sudo apt install valgrind

__Usage__

    $ valgrind ./simpletest
    $ valgrind ./btest

__Help__

    $ valgrind --help